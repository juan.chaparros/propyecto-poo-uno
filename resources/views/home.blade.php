@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><b>OPCIONES DEL SISTEMA</b></div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    <div class="col text-left">
                        <a href="{{route('list.productos')}}" class="btn btn-sm btn-success">Producto</a>                    
                        <a href="{{route('lista.cliente')}}" class="btn btn-sm btn-success"><i class="fa-solid fa-user-plus"></i>&nbspClientes</a>                    
                        <a href="" class="btn btn-sm btn-success"><i class="fa-solid fa-file-lines"></i>&nbspFacturas</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection