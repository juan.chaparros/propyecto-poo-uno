@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">CREAR PRODUCTOS</div>

                    <div class="col text-right">
                        <a href="{{ route('list.productos') }}" class="btn btn-sm btn-success">Cancelar</a>
                    </div>
                <div class="card-body">    
                    <form role="form" method="post" action="{{ route('guardar.productos') }}" >
                        {{ csrf_field() }}
                        {{ method_field('post') }}
                        <div class="row">
                            <div class="col-lg-4">
                            <label for="nombre" class="from-control-label">Nombre del producto</label>
                            <input type="text" class="from-control" name="nombre">
                            </div>
                            <div class="col-lg-4">
                            <label for="tipo" class="from-control-label">Tipo producto</label>
                            <input type="text" class="from-control" name="tipo">
                            </div>
                            <div class="col-lg-4">
                            <label for="estado" class="from-control-label">Estado producto</label>
                            <input type="number" class="from-control" name="estado">
                            </div>
                            <div class="col-lg-4">
                            <label for="precio" class="from-control-label">Precio producto</label>
                            <input type="number" class="from-control" name="precio">
                            </div>
                        
                        </div>&nbsp
                        <button class="btn btn-success pull-right" type="submit"  ><i class="fa-solid fa-floppy-disk"></i>&nbspGuardar</button>
                    </form>                
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
