@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><b>CREAR CLIENTES</b></div>

                    <div class="col text-right">
                        <a href="{{ route('lista.cliente') }}" class="btn btn-sm btn-dark">Cancelar</a>
                    </div>
                <div class="card-body">    
                    <form role="form" method="post" action="{{ route('guardar.cliente') }}" >
                        {{ csrf_field() }}
                        {{ method_field('post') }}
                        <div class="row">
                            <div class="col-lg-4">
                            <label for="nombre" class="from-control-label">Nombre del cliente</label>
                            <input type="text" class="from-control" name="nombre">
                            </div>
                            <div class="col-lg-4">
                            <label for="apellidos" class="from-control-label">Apellidos</label>
                            <input type="text" class="from-control" name="apellidos">
                            </div>
                            <div class="col-lg-4">
                            <label for="cedula" class="from-control-label">Cedula</label>
                            <input type="number" class="from-control" name="cedula">
                            </div>
                            <div class="col-lg-4">
                            <label for="direccion" class="from-control-label">Dirección</label>
                            <input type="text" class="from-control" name="direccion">
                            </div>
                            <div class="col-lg-4">
                            <label for="telefono" class="from-control-label">Teléfono</label>
                            <input type="number" class="from-control" name="telefono">
                            </div>
                            <div class="col-lg-4">
                            <label for="fecha_nacimiento" class="from-control-label">Fecha Nacimiento</label>
                            <input type="date" class="from-control" name="fecha_nacimiento">
                            </div>
                            <div class="col-lg-4">
                            <label for="email" class="from-control-label">Email</label>
                            <input type="email" class="from-control" name="email">
                            </div>
                        
                        </div>
                        <button class="btn btn-success pull-right" type="submit"  ><i class="fa-solid fa-floppy-disk"></i>&nbspGuardar</button>
                    </form>        
                            
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
