@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header"><b>LISTA DE CLIENTES</b></div>
        <div class="col text-right">
          <a href="{{route('agregar.cliente')}}" class="btn btn-sm btn-primary"><i class="fa-solid fa-user-plus"></i>&nbsp Nuevo Cliente</a>
        </div>
        <div class="card-body">
          <table class="table">
            <thead>
              <tr>
                <th scope="col"># ID</th>
                <th scope="col">Nombre</th>
                <th scope="col">Apellidos</th>
                <th scope="col">Cedula</th>
                <th scope="col">Dirección</th>
                <th scope="col">Teléfono</th>
                <th scope="col">Fecha nacimiento</th>
                <th scope="col">Email</th>
                <th scope="col">Editar</th>
                <th scope="col">Eliminar</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($cliente as $item)
              <tr>
                <th scope="row">{{$item->id}}</th>
                <td>{{$item->nombre}}</td>
                <td>{{$item->apellidos}}</td>
                <td>{{$item->cedula}}</td>
                <td>{{$item->direccion}}</td>
                <td>{{$item->telefono}}</td>
                <td>{{$item->fecha_nacimiento}}</td>
                <td>{{$item->email}}</td>
                <td>
                    <form action="">
                        <button class="btn btn-warning btn-sm">
                            <i class="fa-solid fa-pen-to-square"></i>
                        </button>
                    </form>
                </td>
                <td>
                    <form action="">
                        <button class="btn btn-danger btn-sm">    
                            <i class="fa-solid fa-trash-can"></i>
                        </button>
                    </form>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection